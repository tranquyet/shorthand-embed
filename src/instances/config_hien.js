/**
 * Create a new instance of ShortHandEmbed code
 */
new ShortHandEmbed({
    /**
     * This is one of the built-in templates we are using for this instance.
     * One instance only can use one template but multi instances can share the same template.
     * If you want more templates, let checkout the core code and create an other one. All the built-in
     * templates locate at src/templates directory.
     */
    template: 'cta-image',
    /**
     * This is the data we will pass to the template. 
     * Each template has a different data structure, so feel free to pass everything you need to the template.
     */
    data: {
        // The title field is helpful in tracking, it represents this instance in GA (where we can generate a report by this title)
        title: "This is an example of CTA image",
        cta_url: 'http://example.com',
        // The assets url (images,videos,...) must start with http (or https).
        img_url: 'https://wallpapercave.com/wp/MLMJQK7.jpg',
        open_new_tab: true,
        width: "100%",
        height: "400px",
        // For mobile only (optional)
        mobile: {
            cta_url: 'http://example.com',
            img_url: 'https://shorthand-embed.s3.eu-west-2.amazonaws.com/assets/config_v1/image-mobile.jpg',
            width: "100%",
            height: "200px"
        },
        // For tablet only (optional)
        tablet: {
            cta_url: 'http://example.com',
            img_url: 'https://shorthand-embed.s3.eu-west-2.amazonaws.com/assets/config_v1/image-mobile.jpg',
            width: "400px",
            height: "300px"
        },
        // Tracking with google tag manager
        GTM_ID: "GTM-PWSBN3F",
    }
});