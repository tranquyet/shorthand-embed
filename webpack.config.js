const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'core.min.js'
  },
  devServer: {
    contentBase: "./dist",
  },
  module: {
    rules:[
      {
        test: /\.twig$/,
        use: {
          loader: 'twig-loader',
          options: {
          },
        }
      }
    ]
  },
  plugins: [
    new CopyPlugin([
      { from: './src/index.html', to:'index.html' },
      { from: './src/instances/*', to:'[name].[ext]' },
      { from: './assets/**/*' },
    ]),
  ],
};
